//Daniel Leszczynski
//Cse 002 Hw 05
//code asks user for amount of hands drawn then tells the probability of getting a four-of-a-kind,3-of-a-kind, 2pair, 1pair
//code also checks if the amount of hands drawn is actually an interger
import java.util.Scanner;
public class Hw05{
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); 
//initialize the counters for each type and the actual probability which is number per type over total hands
    double FourKindCounter = 0;  
    double ThreeKindCounter = 0;
    double TwoPairCounter = 0;
    double OnePairCounter = 0;
    double ProbFour = 0;
    double ProbThree = 0;
    double ProbTwo = 0;
    double ProbOne = 0;
//ask the user to input number of hands                
System.out.println("Input the number of hands to generate:");
Scanner scan = new Scanner(System.in);
boolean numHandsbool = myScanner.hasNextInt();
 double numHands=0;//initialize the number of hands
   //now check if what the user inputted is an interger            
                if (numHandsbool)
                {
                   numHands=myScanner.nextInt();
                }
                else
                {  
                myScanner.next();
                while(!numHandsbool)
                    {
                System.out.println("Please input a valid interger:");
                numHandsbool = myScanner.hasNextInt();
                }
                numHands=myScanner.nextInt();
                    }
     //get a random number 1-52 for each card in the hand and do this numHands times 
      for(int i = 1; i <= numHands; i++){
      int card1 = (int)(Math.random()*(52))+1; 
      int card2 = (int)(Math.random()*(52))+1;
      int card3 = (int)(Math.random()*(52))+1;
      int card4 = (int)(Math.random()*(52))+1;
      int card5 = (int)(Math.random()*(52))+1;
      
      //next while loops check if there are duplicates, if there are, get another random value 1-52  
       while (card2 == card1){
        card2 = (int)(Math.random()*(52+1))+1;
      } 
       while (card3 == card1){     
        card3 = (int)(Math.random()*(52+1))+1;
      }
       while (card4 == card1){ 
        card4 = (int)(Math.random()*(52+1))+1;
      }
       while (card5 == card1){     
        card5 = (int)(Math.random()*(52+1))+1;
      }
       while (card3 == card2){    
        card3 = (int)(Math.random()*(52+1))+1;
      }
      while (card4 == card2){    
        card4 = (int)(Math.random()*(52+1))+1;
      }
      while (card5 == card2){      
        card5 = (int)(Math.random()*(52+1))+1;
      }
      while (card4 == card3){     
        card4 = (int)(Math.random()*(52+1))+1;
      }
      while (card5 == card3){      
        card5 = (int)(Math.random()*(52+1))+1;
      }
      while (card5 == card4){      
        card5 = (int)(Math.random()*(52+1))+1;
      }
      
        //next divide each card by 13 and measure the remainder
            card1 = card1 % 13;
            card2 = card2 % 13;
            card3 = card3 % 13;
            card4 = card4 % 13;
            card5 = card5 % 13;
      
   //next see if the 2 cards match     
       if (card1 == card2){
        OnePairCounter++;
      }
       if (card1 == card3){
        OnePairCounter++;
      }
       if (card1 == card4){
        OnePairCounter++;
      }
     if (card1 == card5){
        OnePairCounter++;
      }
      if (card2 == card3){
        OnePairCounter++;
      }
      if (card2 == card4){
        OnePairCounter++;
      }
      if (card2 == card5){
        OnePairCounter++;
      }
      if (card3 == card4){
        OnePairCounter++;
      }
      if (card3 == card5){
        OnePairCounter++;
      }
      if (card4 == card5){
        OnePairCounter++;
      }
     
                  //next see if there are 4 that are matching
                      if (card1 == card2 && card1 == card3 && card1 ==card4){
                      FourKindCounter++;
                    }
                    if (card1 == card2 && card1 == card3 && card1 ==card5){
                      FourKindCounter++;
                    }
                    if (card1 == card2 && card1 == card4 && card1 ==card5){
                      FourKindCounter++;
                    }
                    if (card1 == card3 && card1 == card4 && card1 ==card5){
                      FourKindCounter++;
                    }
                    if (card2 == card3 && card2 == card4 && card2 ==card5){
                      FourKindCounter++;
                    }
                  if (card2 == card3 && card2 == card4 && card2 ==card5){
                      FourKindCounter++;
                    }
      
                              //next check if there are two pairs in the hand  
                                if (card1 == card2 && card3 == card4){
                                  TwoPairCounter++;
                                }
                                if (card1 == card2 && card3 == card5){
                                  TwoPairCounter++;
                                } 
                                 if (card1 == card2 && card4 == card5){
                                  TwoPairCounter++;
                                }
                                 if (card1 == card3 && card2 == card4){
                                  TwoPairCounter++;
                                }
                                 if (card1 == card3 && card2 == card5){
                                  TwoPairCounter++;
                                }
                                 if (card1 == card3 && card4 == card5){
                                  TwoPairCounter++;
                                }
                                 if (card1 == card4 && card2 == card3){
                                  TwoPairCounter++;
                                }
                                 if (card1 == card4 && card2 == card5){
                                  TwoPairCounter++;
                                }
                                 if (card1 == card4 && card3 == card5){
                                  TwoPairCounter++;
                                }
                                 if (card1 == card5 && card2 == card3){
                                  TwoPairCounter++;
                                }
                                 if (card1 == card5 && card2 == card4){
                                  TwoPairCounter++;
                                }
                                 if (card1 == card5 && card3 == card4){
                                  TwoPairCounter++;
                                }
                                if (card2 == card3 && card4 == card5){
                                  TwoPairCounter++;
                                }
                                 if (card2 == card4 && card3 == card5){
                                  TwoPairCounter++;
                                }
                                 if (card2 == card5 && card3 == card4){
                                  TwoPairCounter++;
                                   }
        //next check if there are three of a kind
      if (card1 == card2 && card1 == card3){
        ThreeKindCounter++;
      }
      if (card1 == card2 && card1 == card4){
        ThreeKindCounter++;
      }
      if (card1 == card2 && card1 == card5){
        ThreeKindCounter++;
      }
      if (card1 == card3 && card1 == card4){
        ThreeKindCounter++;
      }
      if (card1 == card3 && card1 == card5){
        ThreeKindCounter++;
      }
      if (card1 == card4 && card1 == card5){
        ThreeKindCounter++;
      }
      if (card2 == card3 && card2 == card4){
        ThreeKindCounter++;
      }
      if (card2 == card3 && card2 == card5){
        ThreeKindCounter++;
      }
      if (card2 == card4 && card2 == card5){
        ThreeKindCounter++;
      }
      if (card3 == card4 && card3 == card5){
        ThreeKindCounter++;
      }
    } 
   //calculate the total probability of each  
    ProbFour = (FourKindCounter/numHands);
    ProbThree = (ThreeKindCounter/numHands);
    ProbTwo = (TwoPairCounter/numHands);
    ProbOne = (OnePairCounter/numHands);
 //print all that is required   
    System.out.println("The number of loops:" + numHands);
    System.out.println("The Prob of 4 of a kind:" + ProbFour);
    System.out.println("The Prob of 3 of a kind:" + ProbThree);
    System.out.println("The Prob of 2 pair:" + ProbTwo);
    System.out.println("The Prob of 1 pair:" + ProbOne);
  }  
}