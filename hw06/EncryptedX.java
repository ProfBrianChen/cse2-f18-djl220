//Daniel Leszczynski
//CSE 002 hw06 encryptedX
//due Tues oct 23rd at 11pm
//code asks user for an integer between 0 and 100...checks if the input is correct with constraints...then makes a hidden x
import java.util.Scanner;
public class EncryptedX{
  public static void main(String[] args){  
        Scanner myScanner = new Scanner(System.in);    
        int integer;
    System.out.println("Please enter an integer between 0-100:");//asks for integer between 0 and 100
        while (!myScanner.hasNextInt()) 
            {
            System.out.println("Please enter a valid integer between 0 and 100:");//asks again if not a number
            myScanner.next();
            }
        integer = myScanner.nextInt();
        while (integer > 100 || integer < 0) 
            {
            System.out.println("Please enter a valid integer between 0 and 100:");//asks again if not in range
            integer = myScanner.nextInt();
            }        
    for(int i=0; i<=integer; i++)//counts for all values across up to the inputed value
    {
      for (int j=0; j<=integer; j++)//counts for all values down up to the inputed value
      {
          if (i==j || integer-i==j)//this gives the cross sections that are black without the stars, giving the x desired
            System.out.print(" ");//print the open space if it is...
          else//for all else...
            System.out.print("*");//print stars
    }      
    System.out.println(" ");
    }

}
}