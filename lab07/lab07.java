//Daniel Leszczynski
//CSE 002 Lab07
import java.util.Random;
public class lab07 {
static Random randomGenerator;
public static void main(String[] args){
randomGenerator = new Random();
methodmain();
    }
 //creation of a sentences calling other methods
  public static void methodmain(){
String objectnew = object();        
String subjectnew = subject();
              int num = randomGenerator.nextInt(6);
              System.out.println("The "+subjectnew+" "+verb()+ " the "+adjective()+" "+objectnew+".");             
    for(int i = 0; i < num; i++)
              {
              actSentance(subjectnew);
              }
              System.out.println("The "+subjectnew+" "+conVerb()+" the "+objectnew+"!");
              }
  //this adds filler sentences
  public static void actSentance(String subject){
        int random = randomGenerator.nextInt(1);
                if(random == 0)
                    {
                    System.out.println("It "+verb()+" the "+adjective()+" "+object()+".");
                    } 
                else 
                    {
                    System.out.println("The "+subject+" "+verb()+" the "+adjective()+" "+object()+".");
                    }
}
  //adjective chooser
public static String adjective(){
        int a = randomGenerator.nextInt(9);
        String adj = "";
        switch(a) 
                    {
                    case 1: adj = "big";
                    break;
                    case 2: adj = "sad";
                    break;
                    case 3: adj = "fat";
                    break;
                    case 4: adj = "small";
                    break;
                    case 5: adj = "slow";
                    break;
                    case 6: adj = "active";
                    break;
                    case 7: adj = "serious";
                    break;
                    case 8: adj = "angry";
                    break;
                    case 9: adj = "mad";
                    break;
                    default: adj = "lost";
                    break;
                    }
        return adj;}
//subject chooser
public static String subject(){
        int s = randomGenerator.nextInt(9);
        String sub = "";
        switch(s) {
                    case 1: sub = "deer";
                    break;
                    case 2: sub = "shark";
                    break;
                    case 3: sub = "pig";
                    break;
                    case 4: sub = "goat";
                    break;
                    case 5: sub = "fish";
                    break;
                    case 6: sub = "tree";
                    break;
                    case 7: sub = "boy";
                    break;
                    case 8: sub = "girl";
                    break;
                    case 9: sub = "doctor";
                    break;
                    default: sub = "lizard";
                    break;
                    }
        return sub;}
//verb chooser
public static String verb(){
        int v = randomGenerator.nextInt(9);
        String ver = "";
        switch(v) {
                    case 1: ver= "faked";
                    break;
                    case 2: ver= "slid";
                    break;
                    case 3: ver= "forgot";
                    break;
                    case 4: ver= "drank";
                    break;
                    case 5: ver= "swam";
                    break;
                    case 6: ver = "clipped";
                    break;
                    case 7: ver = "finished";
                    break;
                    case 8: ver = "cleared";
                    break;
                    case 9: ver = "leaped";
                    break;
                    default: ver = "drove";
                    break;
                    }
        return ver;}
//object chooser
public static String object(){
        int obj = randomGenerator.nextInt(9);
        String ob = "";
        switch(obj) {
                    case 1: ob = "donkey";
                    break;
                    case 2: ob = "trout";
                    break;
                    case 3: ob = "goose";
                    break;
                    case 4: ob = "kitten";
                    break;
                    case 5: ob = "squid";
                    break;
                    case 6: ob = "penguin";
                    break;
                    case 7: ob = "boat";
                    break;
                    case 8: ob = "cup";
                    break;
                    case 9: ob = "whale";
                    break;
                    default: ob = "cheetah";
                    break;
                    }
        return ob;}
  //conclusion verb chooser
public static String conVerb(){
        int r = randomGenerator.nextInt(9);
        String con = "";
        switch(r) {
                  case 1: con = "hated";
                  break;
                  case 2: con = "killed";
                  break;
                  case 3: con = "wished";
                  break;
                  case 4: con = "remembered";
                  break;
                  case 5: con = "forgot";
                  break;
                  case 6: con = "loved";
                  break;
                  case 7: con = "ate";
                  break;
                  case 8: con = "avoided";
                  break;
                  case 9: con = "annoyed";
                  break;           
                  default: con = "liked";
                  break;
                  }
        return con;}
}