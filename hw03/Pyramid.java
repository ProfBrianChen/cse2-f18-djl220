//Daniel Leszczynski
//cse 002 hw03 Pyramid.java
// Due 9/18/18 at 11pm
//Program asks for dimensions of pyramid and program prints the calculated volume inside the pyramid
import java.util.Scanner;
public class Pyramid {
  // main method required for every Java program
   	public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
             
              System.out.print("Enter the length of the square side of the pyramid:");//ask for the length of one side of square bottom
              double squareSide = myScanner.nextDouble();//assigned double for the length
                   
                    System.out.print("Enter the height of the pyramid:");//ask for the height of the pyramid
                    double height = myScanner.nextDouble();   //assign double for height
      
      
      
      double volume;//create new double for volume calculation
      volume = (squareSide * squareSide * height) / (3);//calculation of valume of pyramid with square bottom
      
      
  System.out.println("Volume of the pyramid is:" + volume);//print the volume of the pyramid
    
    
    }  //end of main method   
  	} //end of class