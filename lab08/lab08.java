//Daniel Leszczynski
// Lab 08 Arrays
//Code generates random numbers 0-100 100 times and counts the number of iterations of each number 1-100
import java.util.Random;
public class lab08 {
public static void main(String[] args) {
Random ranGen = new Random();
  int[] myList1 = new int[100]; 
//generates random numbers for first list
  for (int i = 0; i < myList1.length; i++) {
 myList1[i] = ranGen.nextInt(100);           
}
int[] myList2 = new int [100];  
  for (int j = 0; j < myList1.length; j++) {
myList2[j]=0;            
}
for (int k = 0; k < myList1.length; k++){
  myList2[myList1[k]] += 1;
}
//prints values from first random list
  System.out.print("Array 1 holds the following values: \n");
for(int z = 0; z < myList1.length; z++){
System.out.print(" " + myList1[z]);
}
 System.out.println(" \n ************");
//prints number of iterations 
  for ( int t = 0; t < myList1.length; t++){
  System.out.println(t + " occurs " + myList2[t] + " times ");         
}
}
}    