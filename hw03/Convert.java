//Daniel Leszczynski
//cse 002 hw03 Convert.java
// Due 9/18/18 at 11pm
//Program asks user to input number of acres caught in storm and number of inches of rain and program converts to cubic miles
import java.util.Scanner;
public class Convert {
  // main method required for every Java program
   	public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
              System.out.print("Enter the number of acres caught in storm:");//ask the user to input number of acres
              double numAcres = myScanner.nextDouble();//assign double to number of acres
              System.out.print("Enter the amount of rain in inches:");//ask user to input number of inches of rain
              double numInches = myScanner.nextDouble();   //assign double to amount of rain in inches
      
      
      
      
      
      double acreInch;//assign double for acre inch
      acreInch = numAcres * numInches;//calculation of acre inch from inputs
      double cubicMiles;//assign new double for final value in cubic miles
     
      
      
      cubicMiles = acreInch * (2.466 * .00000001);//acre inch to cubic mile conversion calculation
      
      
      
      
      System.out.println("Cubic Miles:" + cubicMiles);//print the number of cubic miles
    }  //end of main method   
  	} //end of class