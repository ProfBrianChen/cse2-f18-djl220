//////
///Daniel Leszczynski/////
////HW02 Section 110/////
//////Due 9/11/18 at 11pm////
public class Arithmetic{
public static void main (String args[]){
    
  
    int numPants = 3; //input for number of pants purchased
    int numShirts = 3; //input for number of shirts purchased
    int numBelts = 3; //input for number of belts purchased

  
              double pantsPrice = 34.98;  //set a price per pair of pants
              double shirtsPrice = 24.99 ;  //set a price per pair of pants
              double beltsPrice = 33.99 ;  //set a price per belt 
              double PaSalesTax=0.06; //set a rate for sales tax
              double TotalCostofPants, TotalCostofBelts, TotalCostofShirts;//declare variables for total Cost of pants,belts, and shirts individually 
              double SalesTaxPants, SalesTaxBelts,SalesTaxShirts;  //declare variables for sales tax pants,belts and shirts individually  
              double TotalBeforeTax, TotalSalesTax, BottomLinePrice;// declare variables for total before tax, total sales tax, and bottom line price for whole order

  
                            TotalCostofPants=numPants*pantsPrice; //calculation for total cost of pants before tax
                            TotalCostofBelts=numBelts*beltsPrice;//calculation for total cost of belts before tax
                            TotalCostofShirts=numShirts*shirtsPrice;//calculation for total cost of belts before tax    
                            SalesTaxPants=TotalCostofPants*PaSalesTax;// calculation of sales tax on pants 
                            SalesTaxBelts=TotalCostofBelts*PaSalesTax;// calculation of sales tax on belts
                            SalesTaxShirts=TotalCostofShirts*PaSalesTax;// calculation of sales tax on shirts 
                            TotalBeforeTax=TotalCostofPants+TotalCostofBelts+TotalCostofShirts; //calculation of total cost of all items before tax
                            TotalSalesTax=SalesTaxPants+SalesTaxShirts+SalesTaxBelts; //calculation for total sales tax of all items
                            BottomLinePrice=TotalBeforeTax+TotalSalesTax; //calculation of total bottom line price
  
      String TotalCostofPantsAsString = String.format ("%.2f", TotalCostofPants);//converts to two decimals
      String TotalCostofBeltsAsString = String.format ("%.2f", TotalCostofBelts); //converts to two decimals    
      String TotalCostofShirtsAsString = String.format ("%.2f", TotalCostofShirts); //converts to two decimals
      String SalesTaxPantsAsString = String.format ("%.2f", SalesTaxPants);//converts to two decimals
      String SalesTaxBeltsAsString = String.format ("%.2f", SalesTaxBelts);//converts to two decimals      
      String SalesTaxShirtsAsString = String.format ("%.2f", SalesTaxShirts);//converts to two decimals
      String TotalBeforeTaxAsString = String.format ("%.2f", TotalBeforeTax);//converts to two decimals
      String TotalSalesTaxAsString = String.format ("%.2f", TotalSalesTax);//converts to two decimals
      String BottomLinePriceAsString = String.format ("%.2f", BottomLinePrice);//converts to two decimals
  
  
            System.out.println("Total cost of pants before tax was $"+TotalCostofPantsAsString+"."); //print for total cost of pants before tax 
            System.out.println("Total cost of belts before tax was $"+TotalCostofBeltsAsString+"."); //print for total cost of belts before tax 
            System.out.println("Total cost of shirts before tax was $"+TotalCostofShirtsAsString+"."); //print for total cost of shirts before tax 
            System.out.println("Total sales tax on pants is $"+SalesTaxPantsAsString+"."); //print for total cost of sales tax on pants
            System.out.println("Total sales tax on belts is $"+SalesTaxBeltsAsString+"."); //print for total cost of sales tax on belts
            System.out.println("Total sales tax on shirts is $"+SalesTaxShirtsAsString+"."); //print for total cost of sales tax on shirts
            System.out.println("Total cost of purchases before tax is $"+TotalBeforeTaxAsString+"."); //print for total cost of purchases before tax
            System.out.println("Total of tax on all items is $"+TotalSalesTaxAsString+"."); //print for total amount of tax
            System.out.println("Bottom line price is $"+BottomLinePriceAsString+"."); //print for total amount due                   
                   }     
                   }