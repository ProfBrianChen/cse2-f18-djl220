//Daniel Leszczynski
//cse 002 lab 2
//9/7/18
//This file records the time elapsed and number of rotations of the front wheel
// We have to acomplish 4 tasks
//a.print number of minutes for each trip
//b. print number of counts per trip
//c.print distance of each trip in miles
//d. print distance of two trips combines
public class Cyclometer {
    	
  // main method required for every Java program
   	public static void main(String[] args) {
        int secsTrip1=480; //seconds for trip 1 input
        int secsTrip2=3220;  //seconds for trip 2 input
        int countsTrip1=1561;  //rotation count for trip 1
        int countsTrip2=9037;  //rotation count for trip 2 
        double wheelDiameter=27.0,  //
       PI=3.14159, //value for pi
       feetPerMile=5280,  // value for feet per mile 
       inchesPerFoot=12,   //value for inches per foot
       secondsPerMinute=60;  //value for seconds per minute
       double distanceTrip1, distanceTrip2,totalDistance;  //   
           System.out.println("Trip 1 took "+//print statement for trip one took how long
                (secsTrip1/secondsPerMinute)+" minutes and had "+
                countsTrip1+" counts.");
           System.out.println("Trip 2 took "+//print statement for trip 2 took how long
              (secsTrip2/secondsPerMinute)+" minutes and had "+
                countsTrip2+" counts.");
              distanceTrip1=countsTrip1*wheelDiameter*PI; // distance of trip 1
        distanceTrip1/=inchesPerFoot*feetPerMile; // distance in miles
        distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//calculation for distance of trip 2 
        totalDistance=distanceTrip1+distanceTrip2;//addition for total distance
              //after this comment, everything is for printing
                 System.out.println("Trip 1 was "+distanceTrip1+" miles");//"trip 1 was xxx miles"
        System.out.println("Trip 2 was "+distanceTrip2+" miles"); //"trip 2 was --- miles"
        System.out.println("The total distance was "+totalDistance+" miles");//"the total distance was --- miles"

    }  //main meathod end  
} //class end


