//Daniel Leszczynski
//cse 002 lab 3
//9/14/18
//Program calculates tip and how much each person has to pay
//user inputs check amount,desired tip, and number of people in the party
import java.util.Scanner;
public class Check {
  // main method required for every Java program
   	public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Enter the origional cost of the check in the form of xx.xx:");
    double checkCost = myScanner.nextDouble();
System.out.print("Enter the percentage tip that you wish to pay as a whole number in the form xx:");
      double tipPercent = myScanner.nextDouble();
      tipPercent /= 100;
      System.out.print("Enter the number of people at dinner:");
      int numPeople = myScanner.nextInt();
      double totalCost;
      double costPerPerson;
      int dollars, dimes, pennies;
      totalCost= checkCost * (1+tipPercent);
      costPerPerson= totalCost / numPeople;
     dollars=(int)costPerPerson;
      dimes=(int)(costPerPerson * 10) % 10;
      pennies=(int)(costPerPerson * 100) % 10;
System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);

    }  //end of main method   
  	} //end of class