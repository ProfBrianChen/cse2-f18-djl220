//Daniel Leszczynski
//CSE 002 Lab 05
import java.util.Scanner;
public class UserInput{
  public static void main(String[] args){  
    Scanner myScanner = new Scanner(System.in);              
   //ask for course number 
    System.out.println("Input the course number:");
boolean CourseNumberbool = myScanner.hasNextInt();
 double CourseNumber=0;           
                if (CourseNumberbool)//check if course number is a valid input
                {
                   CourseNumber=myScanner.nextInt();
                }
                else
                {  
                myScanner.next();
                while(!CourseNumberbool)//while it is not correct...
                    {
                System.out.println("Please input a correct course number:");//if its not, ask for it again
                CourseNumberbool = myScanner.hasNextInt();
                }
                CourseNumber=myScanner.nextInt();//take next value
                    } 
    System.out.println("Input the number of times it meets:");//ask for number of times it meets
boolean NumTimesMeetbool = myScanner.hasNextInt();
 double NumTimesMeet=0;           
                if (NumTimesMeetbool)//check if its a valid input
                {
                   NumTimesMeet=myScanner.nextInt();
                }
                else
                {  
                myScanner.next();
                while(!NumTimesMeetbool)
                    {
                System.out.println("Please enter a valid time:");//ask again if not correct
                NumTimesMeetbool = myScanner.hasNextInt();
                }
                NumTimesMeet=myScanner.nextInt();
                    }
  System.out.println("Input the time the class starts:");//ask for time it starts
boolean TimeStartbool = myScanner.hasNextInt();
 double TimeStart=0;           
                if (TimeStartbool)//check if its a valid time
                {
                   TimeStart=myScanner.nextInt();
                }
                else
                {  
                myScanner.next();
                while(!TimeStartbool)
                    {
                System.out.println("Please input a valid time:");//ask again if not correct
                TimeStartbool = myScanner.hasNextInt();
                }
                TimeStart=myScanner.nextInt();
                    }
    System.out.println("Input the number of students in the class:");//ask for number of students in the class
boolean NumStudentsbool = myScanner.hasNextInt();
 double NumStudents=0;           
                if (NumStudentsbool)
                {
                   NumStudents=myScanner.nextInt();
                }
                else
                {  
                myScanner.next();
                while(!NumStudentsbool)
                    {
                System.out.println("Please input a valid number of students:");//ask again if incorrect
                NumStudentsbool = myScanner.hasNextInt();
                }
                NumStudents=myScanner.nextInt();
                    }
System.out.println("Enter the department name:");//ask the department name for input
boolean DepNamebool=myScanner.hasNext();
    String DepName;
      if (DepNamebool)
                {
                   DepName=myScanner.next();
                }
                else
                {  
                myScanner.next();
                while(!DepNamebool)
                    {
                System.out.println("Please input a valid department name:");//ask again for valid department input
                DepNamebool = myScanner.hasNext();
                }
                DepName=myScanner.next();
                    }
      System.out.println("Enter the Professor's name:");//ask for pressor name as input
boolean ProfNamebool=myScanner.hasNext();
    String ProfName;
      if (ProfNamebool)
                {
                   ProfName=myScanner.next();
                }
                else
                {  
                myScanner.next();
                while(!ProfNamebool)
                    {
                System.out.println("Please input a valid professor name:");
                ProfNamebool = myScanner.hasNext();
                }
                ProfName=myScanner.next();
                    }
   //print all  
    System.out.println("The department name:" + DepName);
    System.out.println("The Professor's name:" + ProfName);
    System.out.println("The Course Number:" + CourseNumber);
    System.out.println("Number of times it meets in a week:" + NumTimesMeet);
    System.out.println("The Time it starts:" + TimeStart);
    System.out.println("Number of students in the class:" + NumStudents);
    
  
  } 
}