//Daniel Leszczynski
//Cse 002 Hw 07
//due 10/30 at 11pm
//code asks user for a string of text then presents a menu
//user chooses from the menu and code continues untill user chooses the option to quit
import java.util.Scanner;
public class hw07{
public static void main(String[] args){
String answer;
String newnewtext;
String finding;
String text=sampleText();
boolean answerbool = false;
  System.out.println("You Entered: " + text);
answer=printMenu(); 
  //code makes sure the user enters an option on the menu, if not they ask again
  //multiple if statements for which option is chosen
  //code reruns after each selection
  while (answerbool==false){
  if (answer.equals("c"))
{
System.out.println("Number of non-white spaces: " + getNumOfNonWSCharacters(text));
    answer=printMenu();
}
else if (answer.equals("w"))
{
System.out.println("The number of words:" + getNumOfWords(text));
answer=printMenu();
}
else if (answer.equals("f"))
{
System.out.println("Instances it occurs: " + findText(text));
answer=printMenu();
}  
else if (answer.equals("r"))
{
System.out.println("Edited text: " + replaceExclamation(text));
answer=printMenu();
}
else if (answer.equals("s"))
{
System.out.println("Edited text: " + shortenSpace(text));
answer=printMenu();
}
else if (answer.equals("q"))
{
System.out.println("You have quit.");
System.exit(0);
}
  else
{
    System.out.println("You did not pick a value off the menu.");
answer=printMenu();
  }
  }
}
  //code asks for sample text from user and returns as text
  public static String sampleText(){
    String text;
    Scanner scan = new Scanner(System.in);
    System.out.println("Enter a sample text:");
     text = scan.nextLine();
    return (text);
  }
  //code prints the menus and stores answer
    public static String printMenu(){
      System.out.println("Menu");
      System.out.println("c - Number of non-white space charaters ");
      System.out.println("w - Number of words");
      System.out.println("f - find text");
      System.out.println("r- Replace all !'s");
      System.out.println("s - shorten spaces");
      System.out.println("q - Quit");
      String answer;
      Scanner scan = new Scanner(System.in); 
      System.out.println("Choose an option: ");
      answer = scan.nextLine();
      return (answer);
      
  }
  //code gets number of non white characters and returns the count of them
  public static int getNumOfNonWSCharacters(String text){
    int count=text.length();
    for(int i=0;i<text.length();i++){
      if (Character.isWhitespace(text.charAt(i))){
       count=count-1;
      }
  }
    return (count);
  }
  //because each word is followed by a space, the number of words will be the number of whitespaces plus 1
  public static int getNumOfWords(String text){
     int count=0;
      String newnewtext;
      newnewtext=text.replace("  "," ");
      System.out.println(newnewtext);
      for(int i=0;i<newnewtext.length();i++){
      if (Character.isWhitespace(newnewtext.charAt(i))){
       count=count+1;
  } 
      }
count=count+1;
return (count);
    }
 //code finds the number of times a certain word is used 
  public static int findText(String text){
    int counter3=0;
    int finder;
    Scanner scan = new Scanner(System.in); 
     String finding;
    System.out.println("Enter a word or phrase to be found: ");
      finding = scan.nextLine();    
     finder=text.indexOf(text);
    while (finder !=-1){
      counter3++;
      text.substring(text.length()-finder,text.length());
    }
    return (counter3);
  } 
   //code simply replaces exclamations with periods using the text.replace tool
  public static String replaceExclamation(String text){
     String newtext;
     newtext=text.replace('!','.');
    return (newtext);
  } 
   //same idea as the exclamation replace but with double spaces turing to single 
  public static String shortenSpace(String text){
      String newnewtext;
      newnewtext=text.replace("  "," ");
    return (newnewtext);
    }

 
  }