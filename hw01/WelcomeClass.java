//////
///Daniel Leszczynski/////
////HW01 Section 110/////
//////Due 9/4/18 at 11pm////
public class WelcomeClass{
public static void main (String args[]){

//Printing the desired outcome
System.out.println("    -----------");//pattern with spaces to look more centered
System.out.println("    | WELCOME |");//Welcome potion
System.out.println("    -----------");//pattern
System.out.println("  ^  ^  ^  ^  ^  ^");//pattern
System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ "); // more pattern
System.out.println("<-D--J--L--2--2--0->");//Lehigh ID portion
System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");  //pattern                
System.out.println("  v  v  v  v  v  v");//pattern
                   }     
                   }