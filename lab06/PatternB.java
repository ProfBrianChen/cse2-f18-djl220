//Daniel Leszczynski
//CSE 002 Lab 05 PatternB
import java.util.Scanner;
public class PatternB{
  public static void main(String[] args){  
        Scanner myScanner = new Scanner(System.in);    
        int integer;
System.out.println("Please enter an integer between 1-10:");//ask for integer
        while (!myScanner.hasNextInt()) 
            {
            System.out.println("Please enter a valid integer between 1 and 10:");//asks again if not number
            myScanner.next();
            }
        integer = myScanner.nextInt();
        while (integer > 10 || integer < 1) 
            {
            System.out.println("Please enter a valid integer between 1 and 10:");//asks again if not in range
            integer = myScanner.nextInt();
            }        
    
        for (int i = integer; i >=1 ; i--) 
        {
            for (int j = 1; j <= i; j++)
            {
                System.out.print(j+" ");
            } 
            System.out.println();
        }
}
}